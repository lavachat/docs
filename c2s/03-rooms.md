# Rooms

Rooms are the main concept on LavaChat. You can compare them as Discord's
guilds, or as XMPP's MUCs.

## SimpleRoom object

Contains a partial room object.

| field | type | description |
| --: | :-- | :-- |
| id | RoomID | the room's identifier |
| name | string | the room's name |
| icon | icon | the room's icon **TODO** |
| owner\_id | user id | the room's owner |
| member\_count | positive integer | the amount of members in a room |

## Room object

This is an extension of the SimpleRoom object with the following fields added:

| field | type | description |
| --: | :-- | :-- |
| roles | list[role] | the room's roles |
| emojis | list[emoji] | the room's emojis |
| members | list[member] | list of members in the room |
| channels | list[channel] | list of channels in the room |
| presences | list[presence] | list of presences in the room |

## DMRoom object

Represents a Direct Message Room with another user (the "peer user").

| field | type | description |
| --: | :-- | :-- |
| id | RoomID | the room's identifier |
| recipient | User | the DM room's peer user |

## `POST /rooms/join/<invite>`

Join a Room using an invite. 

Returns:
 - 204 if the invite has been acknowledged.
 - 403 if the invite is unknown or the user has been banned.

## `POST /dm_rooms/start/<user_id>`

Start a DMRoom with another User. You must be friends with another user to
start a DM. Relationships are explained in future sections.

## `GET /rooms/list`

List all the rooms the current authenticated User is in.

Returns a list of SimpleRoom objects.

## `GET /dm_rooms/list`

List all the DM Rooms the current authenticated User is in.

Returns a list of DMRoom objects.

## `DELETE /rooms/<room_id>/@me`

Leave a Room. Returns an empty response (with 204 status code).

If the `room_id` points to a DMRoom the DMRoom will be removed from your DMRoom
list, but it won't be actually deleted.
For actual deletion, look over `POST /dm_rooms/<room_id>/delete`.

## `POST /dm_rooms/<room_id>/delete_request`

Delete a DMRoom when both users confirm the deletion.
