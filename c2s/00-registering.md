# Registering an account

## `POST /auth/register`

Register a new user on a Magma server.

### Input

| field | type | description |
| --: | :-- | :-- |
| username | string | The username of the user |
| email | string | The email of the user |
| password | string | The password of the user |

### Output

| field | type | description |
| --: | :-- | :-- |
| id | UserID | the UUID of the registered user |
