# Querying other users

Users can be represented in two forms:
 - By an alias: `username@server.tld`
 - By their UUID: `@uuid:server.tld`

## User mapping

| field | type | description |
| --: | :-- | :-- |
| id | UserID | the User's UUID |
| aliases | List[UserAlias] | aliases for the User |
| nick | string | the User's preferred nickname |
| avatar | Icon | the User's avatar **TODO** |
| bot | boolean | if the User is a bot user |

## PrivateUser mapping

Contains the same fields as the User mapping, but with private
information related to the User in question.

No other User other than themselves can see their PrivateUser mapping.
Servers MUST guarantee proper access control before revealing a
PrivateUser.

| field | type | description |
| --: | :-- | :-- |
| username | string | the User's username |
| email | string | the user's email address |
| verified | boolean | if the email has been verified |
| theme | Theme | the user's theme preferences |


## `GET /users/@me`

Get yourself as a PrivateUser. Returns PrivateUser.

## `PATCH /users/@me`

Update user information. Accepts a subset of PrivateUser. Returns PrivateUser.
The subset excludes the `verified` and `id` fields as being editable.


## `GET /users/<user_id>`

Get another User. Returns a User mapping, or `null` if no User is found.

## `GET /users/search`

Search for users in the network.

### Input (query parameters)

| name | description |
| --: | :-- |
| q | search parameters. recommended type is Union[UserAlias, UserID] |

### Output

Output is a list containing User objects.
