# Room channels

A channel represents a place on where to send messages to.

DMRooms are considered channels (as they only have one channel representing
the direct message).

## RoomChannel object

| field | type | description |
| --: | :-- | :-- |
| id | ChannelID | Channel ID |
| type | ChannelType | Channel type |
| name | string | Channel name |
| overwrites | mapping[id, ChannelOverwrite] | Channel permission overwrites |
| topic | string | Channel topic |
| warnings | list[string] | List of warnings set by a User for this Channel |

### `ChannelType` enum

| type | value | description |
| --: | :-- | :-- |
| `TEXT` | 0 | A room's text channel |

### `ChannelOverwrite` object

| field | type | description |
| --: | :-- | :-- |
| allow | permissions | the permissions allowed in the overwrite |
| deny | permissions | the permissions denied in the overwrite |
| target | OverwriteTarget | the overwrite's target type |

### `OverwriteTarget` enum

| value | description |
| --: | :-- |
| `"user"` | overwrite targeted at a single User |
| `"role"` | overwrite targeted at a single Role |

## `GET /channels/<channel_id>`

Get a channel. Returns RoomChannel or DMRoom object.

## `PATCH /channels/<channel_id>`

Modify a Room channel, if the currently authenticated User can.

Input is a partial RoomChannel object. Excluded fields are `id` and `type`.

Output is the modified RoomChannel object.

## `DELETE /channels/<channel_id>`

Delete a Room channel. Returns empty response 204.
