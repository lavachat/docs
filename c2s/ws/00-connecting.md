# Connecting to the websocket

The websocket in Magma is used to dispatch real-time events to the user. It is
long-lived and the first thing a client should do upon startup (considering
the client already acquired its authentication token).

From the base URL for `magma_ws_<num>` can receive other parameters
that are specific to the websocket connection you want to start. Those
are set in the query parameters in the given url.


## Websocket URL parameters

| parameter name | description |
| --: | :-- |
| encoding | The encoding of messages in the websocket |
| compression | Compression method for the websocket messages |

### Valid `encoding` values

The default value for `encoding` when it isn't given is `json`.

| value | description |
| --: | :-- |
| `json` | JSON encoding |
| `msgpack` | [MessagePack](https://msgpack.org/) binary encoding |


### Valid `compression` values

**TODO**


## Websocket messages

Websocket payloads will always be a dictionary/mapping.

| field | type | description |
| --: | :-- | :-- |
| op | integer | the operator code of the packet / payload |
| d | Optional[any] | the data of the packet / payload, specific to the packet |


### OP code table

| number | name | description |
| --: | :-- | :-- |
| 0 | Identify | Authenticate to the websocket |
| 1 | Resume | Resume an existing websocket's session |
| 2 | Connected | When a new session is successfully created |
| 3 | Resumed | When a session is moved to the current websocket |
| 4 | ResumeFail | failure to resume a session |
| 5 | Dispatch | Event dispatching **TODO** |
| 10 | Heartbeat | Heartbeat request |
| 11 | Heartbeat ACK | Heartbeat response |
| 20 | CapNegFail | capability negotiation failure |

| op code | sent by |
| --: | :-- |
| 0 | client |
| 1 | client |
| 2 | server |
| 3 | server |
| 4 | server |
| 5 | server |
| 10 | any |
| 11 | any |
| 20 | server |


## Websocket flow

 - Connect to the websocket
 - If no session IDs are stored, send `Identify` with a stored token.
 - If there was a stored session ID, send `Resume` with the token and session id
 - If `Connected` / `Resumed`, start heartbeating with the websocket
 - Wait for incoming events **[TODO]**


## Identify

The first message sent in the websocket.

If the server accepts the token, it will send a `Connected` payload back.

| field | type | description |
| --: | :-- | :-- |
| token | string | The token to authenticate with the websocket |
| caps | CapabilityRequest | websocket extensions |

 - The `caps` field is more throroughly explained in section 01.


## Resume

The first message sent in the websocket if the client wants an already existing
session to move to the current websocket.

| field | type | description |
| --: | :-- | :-- |
| token | string | Token to authenticate with |
| session\_id | string | Session id to resume with |
| from\_seq | integer | Resume from the given sequence's point in time |

Note that `from_seq` and sequence numbers in general are relative to the
session and managed by the server at will.


## Connected

Sent by the server when the client successfully connected. Check the addendum
for error codes when the client did not successfully connect.

 - The `users` list's purpose is to decrease data duplication so that other
    fields only need to provide respective UserIDs.

| field | type | description |
| --: | :-- | :-- |
| hb\_tp | integer | heartbeat parameter. explained better on heartbeat section |
| session\_id | string | session id, used to resume |
| user | PrivateUser | the client's user information |
| users | list[User] | users the client might want to know information on |
| relationships | list[Relationship] | relationships the user has |
| rel\_requests | list[RelationshipRequest] | relationship requests |
| caps | list[CapID] | capabilities for the connection |

 - The `caps` field is more throroughly explained in section 01.

## CapNegFail

*documented in section 01.* capability negotiation logic is purposefully
left out of this section.

## Resumed

Sent when the client successfully resumed. Does not contain the `d` field.


## Heartbeating with the websocket

Heartbeating may only happen AFTER connecting or resuming the session.
Heartbeats take a different form than using the websocket `PING` frames, to
account for reverse proxies that might send a reply back while not properly
sending the frame to the server.

After successfully connecting, the client MUST start heartbeating with the
server periodically. If the client fails to do that, the server will treat the
connection as unstable and may close it at will.

When heartbeating, the client MUST send Heartbeat packets every set amount
of milliseconds. The set amount is determined by the server on the Connected
payload's `hb_tp` property (shorthand for "heartbeat timeperiod").

The client CAN send a Heartbeat back to the server to check its own latency
with the server, instead of waiting for a Heartbeat ACK at the next available
time period.

### Heartbeat

**NOTE**: The `seq` field is REQUIRED when the client is doing a periodic
heartbeat. It is OPTIONAL when not.

| field | type | description |
| --: | :-- | :-- |
| seq | Optional[integer] | last known seq on the client's view |

### Heartbeat ACK

Does not contain the `d` field.
