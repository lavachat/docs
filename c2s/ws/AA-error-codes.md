# Websocket error codes

| error code | description |
| --: | :-- |
| 4000 | General error |
| 4001 | Invalid payload, represents an error in decoding the payload |
| 4002 | Identify failure, the client must close the websocket and redo login logic |
| 4003 | Invalid Heartbeat, the client sent Heartbeat before identifying/resuming |

## Recovery strategies

 - The strategy `reconn` specifies the client should reconnect and try to
    resume if possible (most common).
 - The strategy `stop` specifies the client should stop connecting to the
    websocket and display an error to the user.
 - The strategy `identify` specifies the client should try connecting to the
    websocket then identifying, not resuming, resuming after that will give
    a ResumeFail reply.

| error code | recovery strategy |
| 4000 | reconn |
| 4001 | reconn |
| 4002 | stop |
| 4003 | identify |
