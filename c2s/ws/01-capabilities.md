# Capability Negotiation

Capabilities can be set on the websocket level to extend the protocol in
informal ways. CapIDs should be relatively unique and human-recognizable.

## CapID type

Its a string that defines an extension to the websocket.

The recommended format for CapIDs is
`<3-5 letters identifying the author>/<extension prefered name>`.

Examples of non-existing extensions following the format:
 - `lnx/room-optimization`
 - `slc/ignore-presence`

## CapabilityRequest object

If the server does not support a capability given in the `required` category, it
MUST send a CapNegFail payload in response, with the unsupported capability
in the `unsupported` field in the payload.

| field | type | description |
| --: | :-- | :-- |
| required | list[CapID] | capabilities the client requires for the connection |
| wanted | list[CapID] | capabilities the client wants for the connection |

## CapNegFail

Sent by the server when the client's requested/required capabilities are
not supported / conflicting.

The client is welcome to send a correct Connect payload to retry capability
negotiation.

Note that when a client asks for an example capability C1 (in the `wanted` field
of the CapabilityRequest payload) BUT the server does not support it,
it can send a Connected payload without C1 on the list of capabilities
OR send a CapNegFail describing C1 as unsupported.
Both of those behaviors are accepted. Rationale is that C1 being in the "wanted"
category means that the server can treat as it wants.

| field | type | description |
| --: | :-- | :-- |
| unsupported | list[CapID] | unsupported capabilities |
| conflicts | list[list[CapID]] | capabilities that are conflicting with eachother |

**NOTE**: given capabilities are for example purposes ONLY.

Example:
```javascript
{
    op: 20,
    d: {
        unsupported: [
            'xyz/extension-1', 'xyz/extension-2', 'mnm/push-notifications'
        ],
        conflicts: [
            ['lct/room-optimizations-1', 'lct/room-optimizations-2'],
            ['abc/fast-friends', 'def/fast-friends', 'ghi/fast-friends']
        ]
    }
}
```

## `GET /exts/ws/list`

List supported extensions.

**TODO**
