# Logging in

## `POST /auth/plain`

Logging in with plain credentials to the server

### Input

| field | type | description |
| --: | :-- | :-- |
| username | string | The username of the user |
| password | string | The password of the user |

### Output

Clients must treat the `token` as an opaque string and should not
try to understand any data in it.

| field | type | description |
| --: | :-- | :-- |
| token | string | the authentication token for the user |
