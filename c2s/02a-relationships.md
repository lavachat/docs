# Relationships

Users can have two kinds of relationships: friendships or User blocks.

## Relationship object

| field | type | description |
| --: | :-- | :-- |
| type | RelationshipType | relationship type |
| user\_id | UserID | the user id being referred by the relationship |
| user | Optional[User] | user object |

## RelationshipType enum

| value | description
| --: | :-- |
| 0 | a friendship |
| 1 | a block |

## RelationshipRequest object

| field | type | description |
| --: | :-- | :-- |
| user\_id | UserID | the user id requesting a friendship |
| user | User | the user data |

## RequestType enum


| value | description
| --: | :-- |
| 0 | incoming friend request |
| 1 | outgoing friend request |

## `GET /relationships/@me`

Get all relationships the current authenticated User has.
Returns a list of Relationship objects.

## `GET /relationships/@me/requests`

Get all relationship requests for the currently authenticated User.
Returns a list of RelationshipRequest objects.


## `PUT /relationships/@me/requests/<user_id>`

Accept a friend request. Returns empty response(status code 204).

## `POST /relationships/<user_id>`

Create a relationship / relationship request.

Accepts a Relationship object without the `user_id` or `user` fields.

Returns empty response (status code 204).
