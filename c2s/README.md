# Magma API

There are two parts to Magma's API, the HTTP API and the Websockets API.

The HTTP API deals with actions or queries a client wants to make.

The websockets API is focused on transmitting real-time events to a client
(such as new relationship requests, new messages, etc).

Errors and error codes are on `common/AA-errors.md`, the Websockets API
has its own error codes in `c2s/ws/AA-ws-errors.md` (**TODO**).

## Clients connecting to the Magma API

Clients shall first connect to the websocket API. Once connected, they may use
the HTTP API to do actions.
