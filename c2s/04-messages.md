# Messages

**TODO:** message list

## Message object

| field | type | description |
| --: | :-- | :-- |
| id | MessageID | message id |
| room\_id | RoomID | room id |
| channel\_id | Nullable[ChannelID] | channel id |
| author\_id | UserID | message author id |
| data | MessageData | message data |

## MessageData object

 - `edited_timestamp` is `null` if the message hasn't been edited.

| field | type | description |
| --: | :-- | :-- |
| timestamp | ISO8601 timestamp | message timestamp |
| edited\_timestamp | Nullable[ISO8601 timestamp] | message edit timestamp |
| content | string | message content |
| recipients | list[Recipient] | mentioned actors in a message |
| attachments | list[Attachment] | message attachments **TODO** |

## Recipient object

| field | type | description |
| --: | :-- | :-- |
| type | RecipientType | recipient type |
| id | Union[UserID, RoleID, RoomID] | recipient id |

## RecipientType enum

| value | description |
| --: | :-- |
| 0 | user is the recipient |
| 1 | role is the recipient |
| 2 | room is the recipient |

## `POST /messages/<channel_id>/create`

Create a message in a channel. Dispatches respective events via the websocket
to users of a channel.

The `channel_id` can be both a ChannelID or a DMRoom's RoomID.

The input is a Message object without the `id` and `author_id` field.
Output is a full Message object.

## `PATCH /messages/<message_id>`

Edit a message. Input is a partial MessageData object. Output is a full Message
object.

## `DELETE /messages/<message_id>`

Delete a message. Outputs empty body 204 on success.
