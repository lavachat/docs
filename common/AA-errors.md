# Error codes on HTTP

Those error codes apply both to Magma's API and the LavaChat protocol.

Error messages are sent on non-200 HTTP status codes.

## Error payload structure

```javascript
{
    // this is an optional field
    error: true,

    // also optional
    unexpected: true,

    // exists if error is true
    code: integer,

    // exists if error or unexpected are true
    message: string
}
```

## Error code types

| range | type |
--- | ---
1xx | General errors
2xx | Federation errors

## Error codes


### 1xx

| code | description |
--- | ---
100 | User not found


### 2xx

Note: *"Peer"* represents an external server that is receiving the request.

| code | description |
--- | ---
200 | Error connecting to the external server
201 | Signature error
202 | Server is blocked by peer
203 | Peer is blocked by server
