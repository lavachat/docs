# Connecting to a LavaChat instance

## Wellknown URL

The wellknown URL for lavachat is `.well-known/lavachat`. It gives
paths to contact the services (API or protocols) implemented in a given server.

### Wellknown fields

| field name | type | description |
| --: | :--  | :-- |
| `c2s_apis` | Mapping[C2SIdentifier, string] | Point C2S API strings into base URLs |
| `s2s_apis` | Mapping[S2SIdentifier, string] | Point protocol strings into their base URLs |

### `C2SIdentifier`

C2S Identifiers SHOULD follow a given format, `<identifier>:<number>`, where
`<identifier>` is an identifier for the given C2S API, and `<number>` should be
incremented as incompatible changes are given to the C2S API.

The C2SIdentifier used for Magma API is `magma_api:<number>`, and
`magma_api_ws:<number>` for its websocket API.

### `S2SIdentifier`

S2S Identifiers follow the same recommendations as C2S Identifiers.

The S2SIdentifier used for Lavachat S2S endpoints is `lavachat:<number>`. The
Server Inbox for S2S is defined in its own document.


### Wellknown response example

```json
{
    "c2s_apis": {
        "magma_api:0": "https://example.tld/api",
        "magma_api_ws:0": "wss://example.tld/api/ws"
    },
    "s2s_apis": {
        "lavachat:0": "https://example.tld/_/lavachat",
        "lavachat_inbox:0": "wss://_lavachat.example.tld"
    }
}
```

LavaChat servers and clients MUST cache the wanted base URLs to not stress
the receiving server.

## Steps to connecting to a given server (represented as `server.tld`)

 - Try to request the `https://server.tld/.well-known/lavachat` URL if it isn't
    cached.
 - If it fails, the requesting server can show an error, while caching it to
    prevent spam of non-lavachat servers. Stop here.
 - If it *doesn't*, cache the result and use it to make your proper requests.
