# Client theme preferences

There may be cases where Users want to synchronize theme settings between
their devices/clients. Clients can have differing interfaces, so there can't be
a way to define a single theming structure.

## `Theme` structure

`Theme` is a mapping from `ThemeKey` to a generic mapping. The generic mapping's
structure is something left for client implementations to decide.

It is advisable for C2S APIs to serve this information back to clients, and to
make clients able to edit the information for their `ThemeKey`.

**TODO: client initialization and fetching themes**

## `ThemeKey`

A string in the format `<client_name>:<DeviceID>`

**TODO: `DeviceID`**
