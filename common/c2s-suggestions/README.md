# C2S suggestions

**Documents here are non-normative.**

Documents here describe suggestions for C2S APIs to follow. C2S APIs don't need
to follow the guidelines described here.
