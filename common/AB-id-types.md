# ID types

## `stripped-uuid`

The `stripped-uuid` type is a hexadecimal representation of an UUID,
as a string, without the `-` character.

Example:
 - The UUID `3d4be4ce-70b8-409c-bbf2-8bfb75e46e65` becomes the stripped-uuid
    `3d4be4ce70b8409cbbf28bfb75e46e65`

## `server_url`

`server_url` is the main entrypoint for LavaChat. Keep in mind the `server_url`
may be different than the actual URL LavaChat servers will access. Look over
section 01 for more information.

## `UserID`

Follows the format `@<stripped-uuid>:<server_url>`.

Examples:
 - `@3d4be4ce70b8409cbbf28bfb75e46e65:example.com`

## `UserAlias`

`UserAlias` follows the format `<valid_alias>@<server_url>`.

`valid_alias` represents any combination of characters in the defined alphabet.

**TODO: valid characters**

Examples:
 - `alice@a.com`
 - `b@example.org`

## `RoomID`

`RoomID` follows the format `<stripped-uuid>:<server_url>`

## `ChannelID`

`ChannelID` follows the format `#<stripped-uuid>:<server_url>`

## `RoleID`

`RoleID` follows the format `<stripped-uuid>:<server_url>`

## `MessageID`

`MessageID` follows the format `<stripped-uuid>:<server_url>`
