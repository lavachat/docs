# Semantics for both C2S and S2S

All servers and clients MUST communicate via HTTP with TLS, be it via C2S or
S2S.

All requests and response parameters can take form in query arguments or
JSON encoded bodies unless otherwise specified.
