# Ratelimits

Servers MUST implement ratelimiting strategies to prevent spamming. Clients and
other Servers (requesters) that do not respect the given ratelimits will
have consequences such as IP banning.

When a requester is doing any kind of request to another server,
they MUST respect the given ratelimit headers.

If a requester uses up the Server's ratelimiting, the Server MUST send a
response with a status code of 429.
Clients should stop any other requests, which requests to stop is dependent
on the headers given in that response (especially if `x-ratelimit-id` is 0).

## Ratelimit Headers

| header | type | description |
| --: | :-- | :-- |
| x-ratelimit-id | BucketID | the id of the ratelimit |
| x-ratelimit-bucket-param | boolean | if the bucket uses rightmost parameter as key |
| x-ratelimit-limit | integer | amount of requests that can be done on bucket |
| x-ratelimit-remaining | integer | amount of requests that are available |
| x-ratelimit-reset | unix timestamp | when the ratelimit will be reset |

The purpose of `x-ratelimit-bucket-param` is given at the definition of the
`Endpoint` type.

### `BucketID`

BucketID is an integer, it can be negative.

## Handling ratelimits

The guideline for requesters is to not cause any 429 responses from the Server.
Requesters MUST check the given headers and stop doing any requests when, for
example, `x-ratelimit-remaining` reaches 0.

`x-ratelimit-id` specifies which bucket a requester hit with a given request.
The BucketID 0 specifies a global ratelimit. Other routes CAN have their own
buckets, that is left to the server's bucket list.

**Requesters MUST NOT hardcode mappings from endpoints to buckets. or hardcode
the bucket values themselves.**

## `GET /rtl/buckets`

The Server's bucket list. Requesters CAN use this to populate internal
structures (such as an endpoint to bucket mapping) ahead-of-time.

Please note that the bucket list might not be a complete source of truth
regarding buckets. Take for example a profile change endpoint that has two
ratelimits: one for the profile change itself, other for avatar changes. A
single endpoint could show itself as being part of two buckets. While the first
bucket would be always shown in `x-ratelimit-id`, the avatar change bucket would
only be shown once the requester actually hits that ratelimit, since
`x-ratelimit-id` only shows a single BucketID.

### Output

| field | type | description |
| --: | :-- | :-- |
| buckets | List[BucketMapping] | list of buckets |

#### `BucketMapping`

| field | type | description |
| --: | :-- | :-- |
| bucket\_id | BucketID | bucket id |
| endpoint | Endpoint | endpoint |

#### `Endpoint`

The `Endpoint` type is a string following a specific format:

`<method>,<endpoint_path>`, where `<method>` can be any of the HTTP methods:
`GET`, `POST`, `PUT`, `DELETE`, `PATCH`, etc. `<endpoint_path>` is a simplified
view of a given endpoint.

`<endpoint_path>` is the endpoint's path, with parameters converted into
`<<prefix>_id>`, where `<prefix>` is the parameter's prefix, example:

`GET /users/@user_id:instance.tld` gets converted into
`GET,/users/<user_id>`.

If a given bucket has `x-ratelimit-bucket-param` set to `true`, then the bucket
uses the right-most parameter in the route as a key. That gives the idea of a
"per-room/per-channel ratelimit", example:

Consider the Endpoint `PATCH,/rooms/<room_id>/channels/<channel_id>`. While it
falls under a single BucketID (one for editing a channel), it isn't reasonable
to apply that single bucket to all channels in the user's view. Hence why
requests done with that endpoint would give `x-ratelimit-bucket-param` a `true`
value.
