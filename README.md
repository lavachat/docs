# docs

LavaChat Protocol documentation

## Project goals

To provide a good **group** chat experience.

While Discord guilds give a good base set, it was designed to work in a
centralized environment.

XMPP, while giving good extensibility, has MUCs that don't feel friendly for
people coming in from Discord / Discord-like platforms. MUCs however would fit
in perfectly as a replacement for other environments that look like MUCs, such
as Facebook Groups, Whatsapp or Telegram.

Matrix rooms suffer from the problem that the protocol seems to do way too much,
causing high server loads across the entire ecosystem. The rooms still provide
a MUC-like environment, which isn't a good enough model.

Sadly doing a true Discord-like federated platform would lead into the same
trouble Matrix does, where the servers need to do "way too much". This is
caused via Discord's permission system, which is basically a massive ACL-based
system for IM. This has strong drawbacks on scalability. I have seen cases where
a channel slows down to a halt because of 1000 channel overwrites. While Discord
tries to optimize it, it just raises the bar.

## Structure

The specification is composed of two, separate protocols but that share
common semantics:
 - A C2S API, called "Magma API", Magma standing for the reference
    implementation.
 - A S2S protocol, which is "proper LavaChat".

The split between C2S and S2S helps expand the overall protocol, as other people
may want to design their own C2S APIs for any reason.

## Folder structure
 - `/common`: common docs on both C2S and S2S, such as the wellknown path.
 - `/c2s`: The Magma API documentation.
 - `/s2s`: The LavaChat documentation.

## Guidelines for implementations

Preferred reading order:
 - `common/00` gives basics on encodings and things clients should handle from
    the start.
 - From there, continue with `common/01`.
 - Afterwards, you can go to the `c2s` or `s2s` folder, following the sections
    in order. Should give a nice introduction to the protocol / API.
