# Signing Requests

Requests between Servers MUST be signed. Since all connection through
the Servers is already over TLS, there isn't a need to sign the response.

There is a valid argument that the Server receiving a request can use TLS
client authentication using the Requesting Server's certificate, however
considering how there isn't much documentation available on the subject,
and the concern a theoretical implementation might have because of that,
there is an inclination on using HTTP Signatures as described here.

The HTTP Signatures RFC LavaChat uses is
[Draft 10](https://tools.ietf.org/id/draft-cavage-http-signatures-10.html).

HTTP Signatures are used inside LavaChat to provide authentication of
the Requesting Server. All outgoing requests of a Server MUST be signed
and verified by the Responding Server.

Because of that, Servers MUST generate and distribute key material to
other Servers.

The minimal key configuration is a 2048-bit RSA keypair. Servers MUST NOT
use keypairs that have lower bit-sizes, or algorithms that are worse than
RSA.

The signer settings are as follows:
 - keyID: `https://server.tld/_/lavachat/keys/@self`
 - keyAlgorithm: `rsa-sha256`
 - Signed headers: `(request-target)`, `host`, `date`, `digest`

Signatures MUST be stored in a `Signature` header. The `keyID` MUST provide
an actual keypair once a Server issues a `GET` request to it.

## `GET /keys/@self`

Get the running Server's keypair.

### Output

| field | type | description |
| --: | :-- | :-- |
| key\_type | string | Key type, follows same naming as keyAlgorithm |
| key | string | PEM encoded key |
| ttl | KeyTTL | The preferred caching period for the key |

## KeyTTL type

`KeyTTL` is a positive integer, with the minimum value being 30. A recommended
value for the type is 600.

## The `Digest` header

The Digest header is defined in [RFC3230](https://tools.ietf.org/html/rfc3230).

The minimum requirement for the Digest is a SHA-256 hash of the body. Servers
MUST use the minimally required hash function.

Example for a `Digest` header value with the given body:

```
Digest: SHA-256=S0RQl96YsI0gdnSw+VYtarCsXsS1vGx9qbgkI35zaXI=

{"test":"tested"}
```

## Signature Verification: Business logic

Once a request arrives to a Responding Server:
 - The Server verifies the existence of the `Signature` header, if it does not
    exist, the request should fail with a `403 Forbidden`.
 - Fetch the key in `keyID` if it isn't stored, or the current key is expired.
 - Properly verify the signature using your library of choice.
 - If the verification fails and the key comes from the Servers' cache, fetch
    the key once more, if it is identical to the key you had, fail with a
    `403 Forbidden`.
 - At this point, the verification succeedes. Continue the request.
