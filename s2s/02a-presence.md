# User presence

In the last stage of friendship request acceptance, `presence` is sent
as part of the `friend_accept` event.

## Presence structure

| field | type | description |
| --: | :-- | :-- |
| status | PresenceStatus | The status of the user |
| activity | string | The activity of the user. Nullable if no activity |

### `PresenceStatus` enum

| value | description |
| --: | :-- |
| 0 | User is online |
| 1 | User is offline |
| 2 | User is idle |
| 3 | User is in do-not-disturb (DND) |

## Presence example

```json
{
    "status": 0,
    "activity": "playing minecraft"
}
```

## `presence_update` event

Sent by a server to signal presence update of another user.

The server handling this event SHOULD determine the Users to relay this presence
to. A non-extensive list would be:
 - Users that share rooms with the presence User.
 - Users in friendships with the presence User.

Servers SHOULD improve on dispatching of the presence, e.g only a single
presence update event is dispatched to a (user, peer) pair.
Users SHOULD NOT get N redundant presence updates because they share N rooms
with the presence User.

| field | type | description |
| --: | :-- | :-- |
| from | UserID | the user the given presence is about |
| presence | Presence | the presence for the user |

### `presence_update` example

In this example, `b.com` wants to send a presence update to `a.com` about user
`b-id@b.com`:

```
{
    "from": "b-id@b.com",
    "presence": {
        "status": 3,
        "activity": "playing some games"
    }
}
```
