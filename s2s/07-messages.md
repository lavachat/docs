# Messages

Action Guide:
 - To send a message, `POST /message/create`.
 - To edit a message, `PATCH /message`.
 - To delete a message, `DELETE /message`.

Reaction Guide:
 - Handle `message_*` events as needed.

**Security Note:** It is not possible to fetch a message via its MessageID
with a `GET` request (with the S2S protocol). All message delivery is done
via the `message_create` event. This does NOT mean Servers should not store
messages in their C2S APIs. Just that other Servers won't be able to fetch
a message via the MessageID.

## Message structure

 - `channel_id` is `null` if the `room_id` references a DM Room.

| field | type | description |
| --: | :-- | :-- |
| id | MessageID | message id |
| room\_id | RoomID | room id |
| channel\_id | Nullable[ChannelID] | channel id |
| author\_id | UserID | message author id |
| data | MessageData | message data |

## MessageData

 - The `Timestamp` type represents an ISO8601 timestamp.
 - `edited_timestamp` is `null` if the message hasn't been edited.

| field | type | description |
| --: | :-- | :-- |
| timestamp | Timestamp | message timestamp |
| edited\_timestamp | Nullable[Timestamp] | message edit timestamp |
| content | string | message content |
| recipients | list[Recipient] | mentioned actors in a message |
| attachments | list[Attachment] | message attachments **TODO** |

## Recipient structure

Recipients can be thought of as "delivery hints" or as "mentions" in other
chat protocols e.g Discord. They mean a certain group should be notified of
the existence of the given message.

C2S APIs SHOULD act on this information accordingly, be it via
desktop notifications (e.g `notify-send` on linux), push notifications for
mobile clients, etc.

 - Any User can be a recipient, however, if they don't have permissions to
    access the channel, they MUST NOT receive any knowledge of the message's
    existence.
 - Only Roles with the `mentionable` field set as true can be recipients. Any
    User can put a mentionable Role as a recipient.
 - Marking an entire Room as a recipient follows different logic. Please look
    over the "Room as a Recipient" section.

| field | type | description |
| --: | :-- | :-- |
| type | RecipientType | recipient type |
| id | Union[UserID, RoleID, RoomID] | recipient id |

## `POST /message/create`

Called by Servers wanting to send a message to a Room.

Receiving Servers MUST imlpement proper access control given the `author_id`, 
`room_id` and `channel_id`. It is not allowed to send messages to a Room the
Author is not on, or to a Channel the Author does not have permission to.

### Input

| field | type | description |
| --: | :-- | :-- |
| message | Message | message to be sent, without the `id` field |

### Output

| field | type | description |
| --: | :-- | :-- |
| id | MessageID | the message id of the newly generated message if it was valid |

## Room as a Recipient

Marking an entire room as a recipient involves the User being able to have
the necessary permissions. The endpoint described here is different than the
`POST /message/create` one, as it follows OCAP principles.

### `POST /caps/<cap_id>` as `create_room_recipient_message`

Input to this endpoint is a Message structure. The output is a MessageID, if
the given Message input was accepted.


## `message_create` event

The term "Manager Server" represents the Server a Room is originating from.

This event is sent by Manager Servers when a new incoming message is valid. Look
at `POST /message/create` for more details.

When processing this event:
 - If `message.room_id`'s Server URL is not the same as the Server URL
    dispatching the event, drop it entirely.

| field | type | description |
| --: | :-- | :-- |
| message | Message | message being sent |


## `PATCH /message`

Called by Servers that want to edit messages sent by them.

### Input

`message` represents the old message being edited.

Constraints:
 - `new_message.id` MUST be equal to `message.id`
 - `new_message.room_id` MUST be equal to `message.room_id`
 - `new_message.channel_id` MUST be equal to `message.channel_id`
 - `new_message.author_id` MUST be equal to `message.author_id`

If any of the constraints fail, the event is dropped.

| field | type | description |
| --: | :-- | :-- |
| new\_message | Message | new message data |

### Output

| field | type | description |
| --: | :-- | :-- |
| acknowledged | boolean | if the message edit request has been acknowledged (valid) |

## `message_edit` event

Sent by a Manager Server when a valid `PATCH /message` has been made.

The same constraints that apply to `PATCH /message` apply here. Servers
should check correctness of the event before further processing.

| field | type | description |
| --: | :-- | :-- |
| new\_message | Message | new message information |


## `DELETE /message`

### Input

| field | type | description |
| --: | :-- | :-- |
| id | MessageID | message id to be deleted |
| room\_id | RoomID | room id of the channel|

### Output

| field | type | description |
| --: | :-- | :-- |
| acknowledged | boolean | if the message deletion request has been acknowledged (valid) |

## `message_delete` event

When processing this event:
 - Check if the `room_id` matches, as explained in the `message_create` event.

Note:
 - `channel_id` is optional and will not appear in DM Rooms.

| field | type | description |
| --: | :-- | :-- |
| id | MessageID | message id |
| room\_id | RoomID | room id |
| channel\_id | Optional[ChannelID] | channel id |
