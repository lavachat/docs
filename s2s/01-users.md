# Users

Users are the primary structure in LavaChat, since you need at least one User
to have a Room (explained later on).

User updates are explained through a `user_update` event.

## User structure

Given a UserID, the Server can send a given User object, Servers MUST respect
the visibility of fields:

| field | type | description |
| --: | :-- | :-- |
| id | UserID | the user's identifier, unique |
| aliases | list[string] | the user's known aliases, unique |
| nick | string | the user's preferred nickname |
| avatar | url | the user's avatar |
| bot | boolean | if the user is a bot |

| field | visibility / is public? |
| --: | :-- |
| aliases | yes |
| bot | yes |
| nick | no |
| avatar | no |

Fields that are not public MUST NOT be shared via user discovery. Those fields
are only viewed once someone has a friendship with the given User, or shares a
room with the given User.

## `user_update` event

Sent by a Server to signal changes to a User.

The event payload is a full User object.

# User discovery

All Users are discoverable.

LavaChat specifies `GET /discovery` as a way for Servers to discover other
Users in the network.

As UserAlias'es are modifyable by a User, only UserIDs are made to address
a User in events throughout this documentation. UserAlias'es are only made to
find a UserID.


## `GET /discovery`

### Input as query arguments

| field | type | description |
| --: | :-- | :-- |
| q | Union[UserID, UserAlias] | the user to know information about |

### Output

| field | type | description
| --: | :-- | :-- |
| users | List[User] | users found via the given query |


### Example Business flow

 - User `a@a.com` wants to find out if there is a User pointed by `b@b.com`
 - `a.com` requests `GET b.com/discovery?q=b@b.com`
 - `b.com` responds with `b-id@b.com`
 - `a@a.com` now knows `b@b.com` (a UserAlias) points to `b-id@b.com` (a UserID)

### Example payload

 - `b.com` responds with `b-id@b.com`
```
{
    "users": [
        {
            "id": "b-id@b.com",
            "aliases": ["b@b.com", "b2@b.com"],
            "nick": "B",
            "avatar": null,
            "bot": false
        }
    ]
}
```
