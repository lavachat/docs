# User relationships

Users can have two kinds of relationships:
 - friendships
 - blocks

Both relationship types should be stored by the Server the User is on.

For a User to add another User as a friend, they send a friend request to the 
wanted User, if they aren't blocked by the wanted User and
accept the request, the Receiving Server sends a `friend_accept` event.

If a user has another user blocked, it means they do not want any messages
or events coming from the blocked user. If there was an existing friendship
between the users, that friendship is removed (and federated via the
`friend_reject` event).

**Note:** Both Servers should store relationship information about Users. This
only applies to friendships. Block relationships shouldn't be federated to
any Server.

## About state inconsistencies

If events related to a friendship are denied by the target Server
(e.g trying to send a Message to the User, or a Presence), the Server should
assume the Target lost the relationship information and should send a friend
request back as it was a new friendship.

## Making a friend request

Business logic:
 - User `a@a.com` wants to add User `b@b.com` as a friend.
 - `a.com` calls `POST /relationships` with wanted data.
 - `b.com` checks if `b@b.com` has `a@a.com` blocked, if yes, automatically
    send a `friend_reject` event.
 - `b.com` waits for `b@b.com` to accept or deny the given request.
 - Depending on the result of the request, `b.com` sends a
    `friend_accept` or `friend_reject` event back to `a.com`.

## `POST /relationships`

Sent by a server to signal a friend request.

### Input

| field | type | description |
| --: | :-- | :-- |
| from | UserID | User making the request |
| to | UserID | User to be requested |

### Response

 - If `acknowledged` is `true`, the Server waits for
    `friend_accept` / `friend_reject` events.
 - `acknowledged` is `false` when, for example, the User being requested
    explicitly blocked the User making the request.

| field | type | description |
| --: | :-- | :-- |
| acknowledged | boolean | If the request was acknowledged by the Server |

## `friend_accept` event

| field | type | description |
| --: | :-- | :-- |
| from | UserID | User that was requested and is now accepting the request |
| to | UserID | User that made the request |
| user | ExtendedUser | User information from the user specified in `from` field |

### `ExtendedUser` structure

Same as User, but with an extra `presence` field, containing a Presence object.

## `friend_reject` event

| field | type | description |
| --: | :-- | :-- |
| from | UserID | User that was requested and is now rejecting the request |
| to | UserID | User that made the request |

## Example flow

For all intents and purposes, `a@a.com` is the actual UserID, not the User's
alias. Same applies to `b@b.com`.

Assume both users did not block eachother.

 - User `a@a.com` wants to send a friend request to user `b@b.com`

```
POST b.com/_/lavachat/relationships

{
    "from": "a@a.com",
    "to": "b@b.com"
}
```

 - `b.com` replies, and puts the request in the queue for `b@b.com`.

```
{"acknowledged": true}
```

 - When `b@b.com` accepts the friend request, `b.com` sends an event to `a.com`

```
{
    "type": "friend_reject",
    "data": {
        "from": "b@b.com",
        "to": "a@a.com",
        "user": {
            "id": "b@b.com",
            "aliases": ["b-alias@b.com"],
            "nick": "User B",
            "avatar": "https://example.com/avatars/123456.png",
            "bot": false,
            "presence": {
                "status": "online",
                "activity": null
            }
        }
    }
}
```

 - If `b@b.com` denies the request, `b.com` sends an event to `a.com`.
 - The `friend_reject` event can also be sent if either user deletes the
    friendship later on.

```
{
    "type": "friend_reject",
    "data": {
        "from": "b@b.com",
        "to": "a@a.com"
    }
}
```
