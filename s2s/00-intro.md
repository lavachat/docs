# Introduction to S2S

Servers communicate with eachother via two methods:
 - Wanting to do an action, such as join a room, via an endpoint.
 - Deliver events, such as a message event, via the Server Inbox.

The endpoints for S2S are explained throughout this documentation. The Server
Inbox is formally documented on its own folder in this documentation.

The Server Inbox part of the protocol is a long-lived websocket connection.

The "endpoint" part of the protocol has two requirements:
 - Requests and Responses MUST be over TLS.
 - Requests MUST be signed as well.
