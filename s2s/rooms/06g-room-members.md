# Group Room Members

Members of a Group Room are Users that are inside the Group Room. They contain
their own nickname inside the Group Room and Roles they have inside the
Group Room.

## Member structure

| field | type | description |
| --: | :-- | :--
| user | MemberUser | User being represented by this member, its a partial user structure |
| nick | string | Nickname for the member, nullable |
| roles | list[RoleID] | List of roles the member holds |

## `MemberUser` structure

**TODO: should `MemberUser` just be `User`?**

| field | type | description
| --: | :-- | :-- |
| id | UserID | User ID |
| avatar | the user's avatar **TODO** |
| bot | boolean | if the User is a bot |

## `PATCH /member`

Change the information about a member inside a Room.

This request is made by Servers to Manager Servers.

Manager Servers receiving this event MUST verify if the User specified in the
`from` field has enough permissions to edit the Member in the `to` field.

Manager Servers dispatch `room_sync` events (specifically synchronizing the
`members` store) back to other Servers when an edit is successful.

| field | type | description
| --: | :-- | :-- |
| from | UserID | User ID making the modifications |
| id | UserID | User ID to modify |
| member | Member, with all optional fields | New data for the given Member |

 - The `member` field does not receive the `user` field specified in the
    Member structure. It is not possible for a User to update a Member's User
    information.
