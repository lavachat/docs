# Room synchronization

Networks are assumed to have problems and Servers are assumed to go down at
random times. Because of that Rooms may have modifications while a certain
Server is down, Room Synchronization aims to solve a subset of this problem
which is aimed at getting room state once a Server is back online.

**Servers MUST implement room synchronization support.**

## Context

Rooms have metadata associated with them, such as Roles, Channels, Members, etc.
For each of those associated metadata, we name them "Stores".

From each store we derive a "version hash" of the Store.

## Defined stores

 - `room` store, using the ID, name, icon and owner ID of the Room.
 - `role` store, using the Room's role list
 - `emoji` store, using the Room's emoji list
 - `member` store, using the Room's member list
 - `channel` store, using the Room's channel list
 - `permission` store, using the Room's permission list

## Store versioning algorithm

Given a structure (mapping or list), generate a JSON representation that
obeys the following parameters
 - Keys are sorted.
 - ASCII encoding of unicode characters is always used.
 - There is no extra space around the `,` and `:` characters.

With that JSON representation of the given structure, run the MD5 hash function
over it. The output is the given store's version.

### Example Python implementation

```py
import hashlib
import json

def hash_value(obj: Union[List, Dict]) -> str:
    dumped = json.dumps(
        obj,
        sort_keys=True,
        ensure_ascii=True,
        separators=(',', ':')
    )

    return hashlib.md5(dumped.encode()).hexdigest()
```

### Test values

```
structure = [1, 2, {"key": 'valué'}]
json_representation = '[1,2,{"key":"valu\\u00e9"}]'
md5_hash = 'c17b355e073ea2f95ebec9eeaf97099a'
```

```
structure = {'test': 'testing', 'test_2': ['a', 'b', 'c']}
json_representation = '{"test":"testing","test_2":["a","b","c"]}'
md5_hash = '15dfb54e7c49096d1c1935ee68dda345'
```

## Room event context

All events connected to a room by having a RoomID in the `from` field
contain three extra fields: `store`, `old_version` and `new_version`.

A Server connected to a Room that isn't hosted on the Server MUST store its
current versions of the Room's stores.

## Room synchronization algorithm

The version of a store on the server is called `current_version`.

 - If `old_version` and `new_version` differ, update `current_version`, applying
    the changes described in the current event.
 - If `current_version` differs from `new_version`, send a `room_sync_request`
    event to the Room asking for updates on the outdated store. Wait for a
    `room_sync` event before processing the event.

## `room_sync_request` event

Request synchronization in one or more stores of a Room.

Receiving Servers MUST verify if the Server sending the request is authorized.
That means checking if there is at least one Member coming from that Server.

Receiving Server can reply with `room_sync` or `room_sync_close` events.

| field | type | description |
| --: | :-- | :-- |
| `to` | RoomID | room to sync |
| `stores` | list[string] | stores to update |

## `room_sync` event

Sent back by a server with room store updates.

**Note:** It is not required to receive a `room_sync` event after a
`room_sync_request` event. Manager Servers can send `room_sync` events for
any reason. Servers, upon receiving a `room_sync` event, MUST update their local
information about the Room.

A common case of a `room_sync` being dispatched without a previous
`room_sync_request` is for general Room updates, e.g creation of a Role/Channel,
Member updating their nickname, etc.

| field | type | description |
| --: | :-- | :-- |
| `from` | RoomID | room that is being synced |
| `stores` | mapping[string, generic\_mapping] | the updated stores |

## `room_sync_close` event

Sent by a server in result of a `room_sync_request` event. Only sent if the
Server that made the `room_sync_request` is unauthorized to synchronize.

Happens if:
 - The Server became blocked.
 - There was only a single member in the Room and they got removed.

| field | type | description |
| --: | :-- | :-- |
| `from` | RoomID | room that is being denied syncing |
