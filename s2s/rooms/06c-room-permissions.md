# Room permissions

Rooms (NOT DMRooms) have a permission system based on OCAP principles.

The permission system is made out of two components:
 - The role list, that gives permission addressing.
 - The permission grant list, that defines the logic in which to grant
    permissions from/to Roles/Users.

There are some basic set of rules:
 - Only the owner of a Room can edit the Administrators of a Room.
 - Only Administrators can edit the permission grant list.

## Role structure

| field | type | description |
| --: | :-- | :--
| id | RoleID | The Role's identifier |
| name | string | The Role's name |
| color | Nullable[positive integer] | The Role's color integer, in RGB |
| hoist | boolean | If the Role grants Members in it a place in the member list |
| mentionable | boolean | If the role can be mentioned by other users |

 - If `color` is `null`, Clients should assume some default color.

## Permission structure

| field | type | description |
| --: | :-- | :-- |
| url | url | OCAP url for a permission |
| from | RoomID | the room tied to the permission |
| type | PermissionType | the type of the permission |
| sources | List[UserID or RoleID] | who is granted the permission |
| targets | Optional[List[PermissionTarget]] | who the permission can be acted on |

**TODO:** URL methods depending of the PermissionType wanted.

## `PermissionTarget` union type

PermissionTarget can be:
 - UserID
 - RoleID
 - ChannelID

## `PermissionType` bitfield

The PermissionType is represented as an integer encoding a bitfield:

| field | description |
| --: | :-- |
| create\_invite | Create an invite for the Room |
| kick\_members | Remove members of the Room |
| ban\_members | Ban members of the Room |
| manage\_channels | Create/Modify/Destroy channels in a Room |
| manage\_room | Modify information about the Room |
| manage\_roles | Modify information about Roles in the Room |
| manage\_emojis | Modify information about Emojis in the Room |
| read\_messages | Read channels in the Room |
| send\_messages | Send a message to a Channel in the Room |
| manage\_messages | Delete messages made by other Members |
| embed\_links | Able to embed URLs into messages |
| attach\_files | Able to send attachments in messages |
| read\_history | Able to read the Channel's history |
| mention\_everyone | Able to send mention notifications to all Members |
| external\_emojis | Able to use emojis in their Messages |
| change\_nickname | Able to change own nickname |
| manage\_nicknames | Modify nicknames of other Members |

**TODO:** valid targets for given permissions, e.g create\_invite does not have
`targets`, or send\_messages does not have `UserID | RoleID` as a target

## Test values

 - The permission number `0` has no permissions.
 - The permission number `2048` has `send_messages` permission.
 - The permission number `67584` has `send_messages` and `read_history`
    permissions.
