# Room logic

There are two types of rooms in LavaChat:
 - Group Rooms
 - Direct Message Rooms

Group Rooms can contain any amount of Users inside them from any servers,
however the Group Room is tied to the server it was created on, and gets its
own RoomID accordingly.

Group Rooms also contain channels that can have messages. This is more
explained in the `Messages` section.

Direct Message (DM) Rooms are separate from Group Rooms as there are only
two users in them. As with Group Rooms, DM Rooms also get their own unique
RoomID.

DM Rooms are unique in the sense that only one DM Room can exist between
two Users.

**Note:** Events that are directly correlated to "broadcasting" to all
users of a room MUST have specific metadata. This is explained in detail
within the Server Inbox documentation.

## Room structure

Rooms are the main communication context in LavaChat and so, contain a lot
of fields.

The Room structure is borrowed from Discord's Guild structure.

| field | type | description |
| --: | :-- | :-- |
| id | RoomID | the room's identifier |
| name | string | the room's name |
| icon | image url | the room's icon |
| owner\_id | UserID | the room's owner |
| admin\_ids | list[UserID] | the room's admins |
| emojis | list[Emoji] | the room's emojis |
| members | list[Member] | list of members in the room |
| channels | list[Channel] | list of channels in the room |
| roles | list[Role] | the role list |
| permissions | List[Permission] | the permission grant list |
| presences | Optional[list[Presence]] | list of presences in the room |

 - Keep in mind `presnces` is something optional and only dispatched in events
    that involve initializing room state, such as `room_create`.

## DMRoom structure

The value for `recipient` is different depending on the context, with the only
constraint being that a certain User will never see their own UserID in the
`recipient` field.

Example: the User `a-id@a.com` will never see a recipient of `a-id@a.com`,
and will always see the "peer user" they're having a DM Room with.

| field | type | description |
| --: | :-- | :-- |
| id | RoomID | the room's identifier |
| recipient | UserID | the DM room's peer user |
