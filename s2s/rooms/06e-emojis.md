# Room emojis

## Emoji structure

| field | type | description |
| --: | :-- | :-- |
| id | EmojiID | Emoji identifier |
| name | string | Emoji name |
| animated | Nullable[AnimatedEmoji] | If the emoji is animated and which format does it use |

 - `animated` is `null` if the emoji is not animated.

## `EmojiID`

A string following the format `<stripped-uuid>`.

## `AnimatedEmoji`

A string with the possible values:

| value | description |
| --: | :-- |
| "gif" | A GIF animated emoji |
| "webm" | A WEBM animated emoji |

## `GET /emojis/<emoji_id>/info`

Get information about an Emoji, returns Emoji object or 404's on emojis
not found / not coming from this instance.

## `GET /emojis/<emoji_id>.<extension>`

Get an emoji's raw image data.
