# Room channels

Rooms can have Channels, Messages are targeted at Channels.

Members can only read/send Messages from/to Channels they have permissions on.
Permission granting and management is explained later on.

Note, however, that the Channel list of a room is public. Any User can see the
list of channels in a Room, however, this does not equate to the User being able
to read Messages from any Channel, as explained before.

## Channel structure

| field | type | description |
| --: | :-- | :-- |
| id | ChannelID | channel ID |
| type | ChannelType | channel type |
| name | string | channel name |
| topic | string | channel topic |
| warnings | list[string] | List of warnings for this Channel |

 - The `warnings` field is supposed to signal Channels that may or may not
    contain sensitive material. C2S SHOULD use those warnings.

### `ChannelType` enum

| type | value | description |
| --: | :-- | :-- |
| `TEXT` | 0 | A room's text channel. |
