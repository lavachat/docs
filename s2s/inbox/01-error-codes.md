# Websocket Error Codes

| code | description |
| --: | :-- |
| 4000 | a generic error has occoured. |
| 4001 | packet decode error |
| 4002 | invalid/unsupported encoding |
| 4003 | packet has invalid operator |
| 4004 | packet sent before authentication |
| 4005 | authentication method in AUTH\_START is incompatible or does not exist |
| 4006 | authentication method selected failed |
| 4007 | already authenticated |
| 4008 | the instance connecting is blocked from federation |
