# Introduction to the Server Inbox

The Server Inbox is the main method Servers dispatch realtime events to
eachother. It is a long-lived websocket connection that servers can use it in
both ways: Server A can dispatch an event to Server B and vice-versa via the
same connection.

The S2SIdentifier's prefix for the server inbox is `lavachat_inbox`.

Since all connections in LavaChat MUST be inside HTTP over TLS, the websocket
connections must also run over TLS. Values for the `lavachat_inbox` identifier
MUST use the `wss://` protocol suffix.

**TODO:** policies in retrying to connect from Starting Server

## Connecting to a Server Inbox

An Initiating Server A wants to dispatch an event E to Receiving Server B.
 - Server A gets the URL for Server B's inbox via the Wellknown URL.
 - Server A starts a websocket connection to Server B's inbox.
 - Server B gives a ConnectionID as the initiating message in the websocket (
    the HELLO packet). It also includes supported authentication methods.
 - Server A selects a supported authentication method, gives the data for
    authentication if any.
 - After authentication is set, events can be sent between the websocket.

## Selecting an encoding

When the Starting Server is creating a websocket with the Receiving Server,
the Starting Server CAN provide another type of encoding. That encoding is
selected via the query arguments given to the websocket URL gathered from
the Wellknown URL.

The Starting Server can insert an `encoding` field and all messages MUST follow
the given encoding.

| value | description |
| --: | :-- |
| json | JSON encoding |
| msgpack | MessagePack encoding |

## Websocket messages

Messages in the websocket follow the encoding defined by the Starting Server.

| field | type | description |
| --: | :-- | :-- |
| op | integer of Operator enum | operation code |
| d | Any | operation data, explained in each operator's field table |
| r | Optional[RoutingData] | room routing |

## `Operator`

| value | name |
| --: | :-- |
| 0 | `HELLO` |
| 1 | `AUTH_START` |
| 2 | `AUTH_ACCEPT` |
| 3 | `DISPATCH` |

## RoutingData object

| field | type | description |
| --: | :-- | :-- |
| t | RoutingMethod | routing method for given packet |
| m | Optional[RoutingMetadata] | routing metadata for given packet |

## HELLO packet

The first packet sent in the connection.

| field | type | description |
| --: | :-- | :-- |
| conn\_id | ConnectionID | connection id |
| heartbeat\_period | positive integer, milliseconds | heartbeating |
| routing\_supported | List[RoutingMethod] | routing methods supported |
| auth\_methods | List[AuthMethod] | authentication methods supported |

### Connection start

Once a Starting Server starts a websocket with a Receiving Server, the
Receiving Server MUST assign a string identifier to the connection and it MUST:
 - Be relatively unique across all other connections
 - Be hard to guess
 - Use randomness in some way

A recommended way to generate a ConnectionID is `SHA-1(<more than 128 bytes>)`.
Consider the hexadecimal digest part of the hash as the actual ConnectionID.

### AuthMethod enum

AuthMethod is a string enum:

| value | description |
| --: | :-- |
| "inbox" | Server Inbox Authentication |
| "tls-cli" | TLS Client Authentication |

Servers MUST support Server Inbox Authentication as a minimum. Servers can
provide TLS Client Authentication as well, and it MUST be preferred to Server
Inbox Authentication. As there are drawbacks when using TLS Client Auth (such
as the lack of documentation or increasing client complexity), it isn't a
requirement.

## AUTH\_START packet

| field | type | description |
| --: | :-- | :-- |
| type | AuthMethod | authentication method |
| routing\_supported | List[RoutingMethod] | routing methods supported |
| data | Any | authentication data |

## TLS Client Authentication

The Starting Server in a connection can specify this (when supported) method
to signal the Receiving Server to check the client certificate in the
connection. The advantage is that you don't need to do any more work in regards
to authentication.

The `data` for TLS Client Authentication is a string, containing the domain the
Starting Server desires to authenticate itself as.

## Server Inbox Authentication (SIA)

Server Inbox Authentication gives the same guarantees as TLS Client
Authentication, as the server signing keys are served via TLS.

SIA works via signing the Starting Server's domain, the Receiving Server's
domain and the ConnectionID sent in the HELLO packet, following the rules:
 - The Starting Server uses the same keys it uses for signing the requests.
 - The value to be signed follows a format: concatenate the Starting Server's
    domain, the Receiving Server's domain and the ConnectionID together, with
    a whitespace between each item.

```
starting_domain = "a.com"
receiving_domain = "b.com"
connection_id = "abc123"

to_be_signed = "a.com b.com abc123"
```

The `data` for SIA is an object:

| field | type | description |
| --: | :-- | :-- |
| originating\_domain | string | the originating domain |
| sig\_type | KeyAlgorithm | the algorithm used to sign the value |
| signature | string | SIA's "authentication value" |

 - Note that `KeyAlgorithm` follows the same rules as the `keyAlgorithm` in
    request signing.

### AUTH\_ACCEPT packet

This does not have data and signals that the authentication method selected
in the AUTH\_START packet has been validated correctly.

## Heartbeating

Heartbeating between the servers can only happen AFTER authentication has took
place and is validated. The Servers SHOULD use the value negotiated by the
Receiving Server in the HELLO packet's `heartbeat_period`.

Heartbeating serves both as a latency and trust metric for P-Multicast (
explained in routing) and as a keepalive indicator of the connection.

### HEARTBEAT packet

This packet carries no data. A Server receiving this packet MUST send a
HEARTBEAT\_ACK packet back with the necessary information in the packet.

### HEARTBEAT\_ACK packet

| field | type | description |
| --: | :-- | :-- |
| boottime | positive integer in seconds | seconds since the server has started |

Note that `boottime` should follow some monotonic cloc inside the Server's
software

## Event dispatching

Once authenticated (by doing Server Inbox Authentication both ways), the
servers can now dispatch any events to eachother.

### `DISPATCH` packet

| field | type | description |
| --: | :-- | :-- |
| type | string | event type |
| data | Any | event data |
