# Room event routing

## Rationale

Room event dispatching to other servers is a tricky solution, where obvious
solutions sometimes don't imply they're the best ones.

One such usecase is XMPP / Jabber, where as a Server hosting a MUC must send
its updates to every user, individually. This has been addressed before by
the PSYC protocol group and with some pseudo-XEPs, such as [Smart Chat] and [Smart Presence],
but they were not accepted by the XSF. We will be using the
nomenclature on those pseudo-XEPs throughout.

[Smart Chat]: http://smarticast.psyced.org/jep-smart-chat.html
[Smart Presence]: http://smarticast.psyced.org/jep-smart-presence.html

The routing metadata is extensible so we can add other methods of room
event routing in the future.

## RoutingData object

| field | type | description |
| --: | :-- | :-- |
| t | RoutingMethod | routing method |

## RoutingMethod integer enum

| value | type | description |
| --: | :-- | :-- |
| 0 | SMART\_UNICAST | smart unicast dispatch method |
| 1 | PSYC\_MULTICAST | PSYC-like multicast |

## Routing Method support

The `HELLO` packet contains the routing methods the Receiving Server supports.
The `AUTH_START` packet contains the routing methods the Starting
Server supports.

All users MUST support `SMART_UNICAST` as a starting point.

## Smart Unicast

Smart Unicast works via the Manager Server dispatching the given room event to
all Servers that have at least one User connected to the Room. When one of those
Servers receive the room event, they dispatch it to the Users via the given
C2S API.

## PSYC-like Multicast

PSYC-like Multicast (shortened here to "P-Multicast") is a Multicast strategy
using groups of Servers that act as routers, dispatching to other Servers,
minimizing the amount of messages the Manager Server has to send.

It uses routing metrics such as latency and boottime to determine grouping
and trust a Server has to be a relay.

The Manager Server groups the other Servers it is currently connected
(via Server Inbox) to via the latency of given servers, determined via the
HEARTBEAT packet. From there, the Manager Server can select a trusted "Relay"
Server based on the boottime (how many seconds since server start) of the
given group. Servers with higher boottime get higher default trust.

Trust is decreased by the amount of room synchronizations that happens after
dispatching. Since room synchronizations are direct with the Manager Server,
they don't require trust on relay servers. Since events related to rooms always
have the required room sync fields, there is always a possibility a server can
decrease the trust of another.

Keep in mind Servers can also purposefully degrade the trust of other Servers by
forcing a room sync request.

In the end result, P-Multicast works *best* when Servers trust eachother and
behave according to this standard.

### Netsplit in P-Multicast

If there is a failure when sending the event to a given relay, the Manager
Server MUST select another Relay in the group to send the event to, recommended
approach is to select the 2nd-most-trusted server, if that fails, go to
the 3rd-most-trusted server, etc...

If all servers in the group fail, the Manager Server MUST still retry
connections, if after a set of time they still fail, mark the servers as
malfunctioning and preferrably signal other Users about the scenario.

### Netjoin in P-Multicast

**TODO**: iterate on "resuming events"?

When a Server that was supposed to be a relay reconnects back to the network,
it MUST synchronize room state, as defined in Room Synchronization.

### RoutingMetadata for P-Multicast

It is an object composed of:

| field | type | description |
| --: | :-- | :-- |
| s | List[string] | list of servers to forward the event to |
| sig | Optional[string] | signature of the event |
| sig\_type | Optional[string] | signature algorithm |

#### Event signatures

To prevent servers acting as relays from modifying the event data, events
can be optionally signed using the same keys used for request signing.

The signature's input is the encoded result of the events' `data`, with the
events' `type` prepended to it.

```
dispatch_type = "room_update"
dispatch_data = "<encoded room object>"

sig_input = dispatch_type + dispatch_data
sig_input = "room_update<encoded room object>"
```

**Note:** the Key used to sign is the same key used to sign requests, so the
key MUST be available via the `GET /keys/@self` endpoint.

**Note:** Due to the computational requirement of running signatures in high
event load scenarios, generation and verification are optional.

Smart Unicast does not provide signatures since TLS takes care of handling
everything else. P-Multicast only provides it due to the nature of relay
servers being able to modify the packet content mid-way.

## Example setup

Suppose a `@room:a.com` room, where two other servers are connected to, `b.com`
and `c.com`, with their own users: `@b:b.com`, `@c:c.com`, `@d:c.com`.

Suppose `b.com` and `c.com` are close geographically, and so the average latency
between them and `a.com` is close.

### Smart Unicast example

On the Smart Unicast method, `a.com` dispatches a room event to `b.com` and
`c.com`, which then dispatch to their Users respectively. `a.com` never
addresses the Users directly, it is the job of `b.com` and `c.com` to determine
the Users receiving the room event.

In the case of room events, Servers MUST dispatch to the users that have
a membership within the room.

In the case of room-**related** events, such as channel based events, Servers
MUST only send the events to Users that *have* permissions on such events.
A guideline is that events such as `message_create` MUST only be sent to Users
that have permissions to read Messages from the channel.

### P-Multicast example

On P-Multicast, `a.com` sends a single event to `b.com` or `c.com`, which then
take care of relaying the event to the other Server. The choice of selecting
`b.com` or `c.com` relies on `a.com`'s trust and latency metrics. Gathered
from Server Inbox's HEARTBEAT packets.

As with Smart Unicast, a Server receiving an event tagged as such will also
receive a list of the other servers it MUST relay to. The Server also dispatches
the given event to its Users via C2S APIs.
